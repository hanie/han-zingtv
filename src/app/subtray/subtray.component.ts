import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-subtray',
  templateUrl: './subtray.component.html',
  styleUrls: ['./subtray.component.styl']
})
export class SubtrayComponent implements OnInit {
  @Input() data: any[]

  constructor() { }

  ngOnInit() {
  }

}
