import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtrayComponent } from './subtray.component';

describe('SubtrayComponent', () => {
  let component: SubtrayComponent;
  let fixture: ComponentFixture<SubtrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubtrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
