import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderSecondComponent } from './header-second/header-second.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterInfoComponent } from './footer-info/footer-info.component';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { SubtrayComponent } from './subtray/subtray.component';
import { SubtrayItemComponent } from './subtray-item/subtray-item.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SectionContentComponent } from './section-content/section-content.component';
import { SectionRowComponent } from './section-row/section-row.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HeaderSecondComponent,
    CarouselComponent,
    FooterInfoComponent,
    TitleBarComponent,
    SubtrayComponent,
    SubtrayItemComponent,
    LoginComponent,
    RegisterComponent,
    SectionContentComponent,
    SectionRowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
