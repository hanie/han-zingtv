import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtrayItemComponent } from './subtray-item.component';

describe('SubtrayItemComponent', () => {
  let component: SubtrayItemComponent;
  let fixture: ComponentFixture<SubtrayItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubtrayItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtrayItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
