import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-subtray-item',
  templateUrl: './subtray-item.component.html',
  styleUrls: ['./subtray-item.component.styl']
})
export class SubtrayItemComponent implements OnInit {
  @Input() data: any[]

  constructor() { }

  ngOnInit() {
  }

}
