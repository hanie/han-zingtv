import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-info',
  templateUrl: './footer-info.component.html',
  styleUrls: ['./footer-info.component.styl']
})
export class FooterInfoComponent implements OnInit {
    items = [
    {
      title: 'ZINGTV',
      name: ['Giới thiệu', 'Tuyển dụng', 'Quảng cáo', 'Thiết bị hỗ trợ']
    },
    {
      title: 'ĐÓNG GÓP',
      name: ['Thành viên VIP', 'Upload']
    },
    {
      title: 'QUY ĐỊNH',
      name: ['Điều khoản sử dụng', 'Chính sách riêng tư', 'Nguyên tắc cộng đồng', 'Bản quyền']
    },
    {
      title: 'BỔ SUNG',
      name: ['APIs', 'Embed', 'RSS', 'Widgets']
    },
    {
      title: 'FOLLOWS US',
      name: ['Zing Me', 'Facebook', 'Youtube', 'Twitter', 'Google+']
    },
    {
      title: 'TRỢ GIÚP',
      name: ['FAQ', 'Liên hệ', 'Báo lỗi']
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
