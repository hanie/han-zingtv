import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-content',
  templateUrl: './section-content.component.html',
  styleUrls: ['./section-content.component.styl']
})
export class SectionContentComponent implements OnInit {
  data = [
    {
      title: "VIDEO MỚI NỔI BẬT",
      data: [
        {
          title: 'Cô Dâu Thủy Thần',
          subtitle: 'Tập 2',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/1/f/1f90aa8cae3c051295c4355708de9420_1499184087.jpg'
          }
        },
        {
          title: 'Đặc Công Hoàng Phi Sở Kiều Truyện',
          subtitle: 'Tập 36',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/2/1/21997a539792eff77e0a6c8a3e99491a_1499375870.jpg'
          }
        },
        {
          title: 'Running Man Bản Trung Quốc',
          subtitle: 'Tập 11',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/4/04421c6c309c6912079e1b5f279e6484_1499270355.jpg'
          }
        },
        {
          title: 'CEO Tài Ba',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/b/2/b251b0580e5de10880ee9fbc07a5d215_1493881632.jpg'
          }
        },
        {
          title: 'Gia Đình Vui Nhộn',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/3/2/325ad04cbad7d043355b6d63da71754c_1497843165.jpg'
          }
        },
        {
          title: 'Hoàng Cung (Ver Thái)',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/c/0c70a7ab1b67ebc2114bd8996a41b42d_1499311825.jpg'
          }
        },
        {
          title: 'Hoàng Cung (Ver Thái) 1',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/c/0c70a7ab1b67ebc2114bd8996a41b42d_1499311825.jpg'
          }
        }
      ]
    },
    {
      title: "HELLO HAN",
      data: [
        {
          title: 'Cô Dâu Thủy Thần',
          subtitle: 'Tập 2',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/1/f/1f90aa8cae3c051295c4355708de9420_1499184087.jpg'
          }
        },
        {
          title: 'Đặc Công Hoàng Phi Sở Kiều Truyện',
          subtitle: 'Tập 36',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/2/1/21997a539792eff77e0a6c8a3e99491a_1499375870.jpg'
          }
        },
        {
          title: 'Running Man Bản Trung Quốc',
          subtitle: 'Tập 11',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/4/04421c6c309c6912079e1b5f279e6484_1499270355.jpg'
          }
        },
        {
          title: 'CEO Tài Ba',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/b/2/b251b0580e5de10880ee9fbc07a5d215_1493881632.jpg'
          }
        },
        {
          title: 'Gia Đình Vui Nhộn',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/3/2/325ad04cbad7d043355b6d63da71754c_1497843165.jpg'
          }
        },
        {
          title: 'Hoàng Cung (Ver Thái)',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/c/0c70a7ab1b67ebc2114bd8996a41b42d_1499311825.jpg'
          }
        }
      ]
    },
     {
      title: "HELLO HAN",
      data: [
        {
          title: 'Cô Dâu Thủy Thần',
          subtitle: 'Tập 2',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/1/f/1f90aa8cae3c051295c4355708de9420_1499184087.jpg'
          }
        },
        {
          title: 'Đặc Công Hoàng Phi Sở Kiều Truyện',
          subtitle: 'Tập 36',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/2/1/21997a539792eff77e0a6c8a3e99491a_1499375870.jpg'
          }
        },
        {
          title: 'Running Man Bản Trung Quốc',
          subtitle: 'Tập 11',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/4/04421c6c309c6912079e1b5f279e6484_1499270355.jpg'
          }
        },
        {
          title: 'CEO Tài Ba',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/b/2/b251b0580e5de10880ee9fbc07a5d215_1493881632.jpg'
          }
        },
        {
          title: 'Gia Đình Vui Nhộn',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/3/2/325ad04cbad7d043355b6d63da71754c_1497843165.jpg'
          }
        },
        {
          title: 'Hoàng Cung (Ver Thái)',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/c/0c70a7ab1b67ebc2114bd8996a41b42d_1499311825.jpg'
          }
        }
      ]
    },
    {
      title: "HELLO HAN",
      data: [
        {
          title: 'Cô Dâu Thủy Thần',
          subtitle: 'Tập 2',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/1/f/1f90aa8cae3c051295c4355708de9420_1499184087.jpg'
          }
        },
        {
          title: 'Đặc Công Hoàng Phi Sở Kiều Truyện',
          subtitle: 'Tập 36',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/2/1/21997a539792eff77e0a6c8a3e99491a_1499375870.jpg'
          }
        },
        {
          title: 'Running Man Bản Trung Quốc',
          subtitle: 'Tập 11',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/4/04421c6c309c6912079e1b5f279e6484_1499270355.jpg'
          }
        },
        {
          title: 'CEO Tài Ba',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/b/2/b251b0580e5de10880ee9fbc07a5d215_1493881632.jpg'
          }
        },
        {
          title: 'Gia Đình Vui Nhộn',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/3/2/325ad04cbad7d043355b6d63da71754c_1497843165.jpg'
          }
        },
        {
          title: 'Hoàng Cung (Ver Thái)',
          subtitle: 'Tập 1',
          avatar: {
            url: 'http://zingtv-photo-td.zadn.vn/tv_media_225_126/0/c/0c70a7ab1b67ebc2114bd8996a41b42d_1499311825.jpg'
          }
        }
      ]
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
