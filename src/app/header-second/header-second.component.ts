import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-second',
  templateUrl: './header-second.component.html',
  styleUrls: ['./header-second.component.styl']
})
export class HeaderSecondComponent implements OnInit {
  categories1 = [
    'Phim Hàn Quốc','Phim Hoa Ngữ',
    'Phim Thái Lan','Phim Việt Nam',
    'Phim Đài Loan','Phim Hồng Kông','Phim Nhật Bản'];
  categories2 = [
    'Phim Âu Mỹ','Hành Động - Phiêu Lưu','Khoa Học - Viễn Tưởng',
    'Hình Sự - Tội Phạm','Tâm Lý - Lãng Mạn','Lịch Sử - Cổ Trang',
    'Kinh Dị - Siêu Nhiên'
    ];
  categories3 = [
    'Giải Trí - Âm Nhạc','Phim Thần Tượng','Phim Gia Đình','Hài Hước',
    'Thể Thao','Live Action','Khác'
  ];
  categories4 = [
    'Phim Trung Quốc','Ngôn Tình','Cung Đấu','Tiên Hiệp','Kiếm Hiệp','Huyền Huyễn'
  ];
  constructor() { }

  ngOnInit() {
  }

}












