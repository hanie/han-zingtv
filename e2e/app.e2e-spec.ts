import { Han2Page } from './app.po';

describe('han2 App', () => {
  let page: Han2Page;

  beforeEach(() => {
    page = new Han2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
